### BoltDB
BoltDB based persistence plugin for VolantMQ

[![pipeline status](https://gitlab.com/volantmq/vlplugin/persistence/bbolt/badges/master/pipeline.svg)](https://gitlab.com/volantmq/vlplugin/persistence/bbolt/master)

## Config
Docker image of VolantMQ service comes with `persistense_bbolt` plugin
To enable plugin add `persistense_bbolt` value to list of enabled plugins. In config section listening port and path can be specified
```yaml
plugins:
  enabled:
    - persistense_bbolt
  config:
    persistence:
      - backend: bbolt
        config:
          file: "/var/lib/volantmq/data/bbolt.db"
```

## Storage structure
```
╔════════════════════════════════════╗                     
║ V: count - total amount of sessions║                     
║ B: sessions - actuall sessions     ║                     
╚═▲══════════════════════════════════╝                     
  │                                                        
  │.n                                                      
╔═╩═══════════════╗      .1 ┌─────────────────────┐        
║ V: subscriptions◀─────────┤Encoded subscriptions│        
║ B: state        ◀────┐    └─────────────────────┘        
║ B: packets      ◀───┐│ .1 ╔═════════════╗                
╚═════════════════╝   │└────╣ V: version  ║                
                      │     ║ V: since    ║                
                      │     ║ V: expireIn ║                
                      │     ║ V: willIn   ║                
                      │     ║ V: willData ║                
┌───────────────┐     │     ╚═════════════╝                
│ Legend        │     │  .1 ╔═════╗.1  .n ╔═══════════════╗
│ V - value     │     └─────╣ Seq ◀───────╣ V: data       ║
│ B - bucket    │           ╚═════╝       ║ V: unAck      ║
└───────────────┘                         ║ V: expireAt   ║
                                          ╚═══════════════╝
```
