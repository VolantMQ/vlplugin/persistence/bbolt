module gitlab.com/VolantMQ/vlplugin/persistence/bbolt

go 1.13

require (
	github.com/VolantMQ/vlapi v0.5.6
	github.com/troian/semver v1.1.0
	go.etcd.io/bbolt v1.3.4
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
